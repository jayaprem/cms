import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  apiUrl = "http://testapi2k18.us-3.evennode.com";

  constructor(private httpClient:HttpClient) { }

  getCustomers(){
    return this.httpClient.get(this.apiUrl+'/customers');
  }

  createCustomer(customerObj){
    return this.httpClient.post(this.apiUrl+'/customers',customerObj);
  }

  updateCustomer(customerObj){
    return this.httpClient.put(this.apiUrl+"/customers/"+customerObj.id,customerObj);
  }

  deleteCustomer(id){
    return this.httpClient.delete(this.apiUrl+"/customers/"+id);
  }

}

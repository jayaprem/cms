import { Component, OnInit } from '@angular/core';
import { CustomersService } from './customers.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  editableRow = null;
  customerList;

  constructor(private custService: CustomersService) { }

  ngOnInit() {
    this.custService.getCustomers().subscribe(res=>{
      this.customerList = res;
    });
  }

  updateCustomer(customerObj,i){
    if(customerObj.id){
      this.custService.updateCustomer(customerObj).subscribe(res=>{
        this.customerList[i] = res;
        this.editableRow = null;
      });
  
    }
    else{
      this.custService.createCustomer(customerObj).subscribe(res=>{
        this.customerList[i] = res;
        this.editableRow = null;
      });
    }
    
  }

  editCustomer(i){
    this.editableRow = i;
  }

  saveCustomer(customerObj){
    /* this.custService.updateCustomer(customerObj).subscribe(res=>{
      this.customerList[i] = res;
      this.editableRow = 5;
    }); */
  }

  deleteCustomer(id,index){
    this.custService.deleteCustomer(id).subscribe(res=>{
      this.customerList.splice(index,1);
    });
  }

  addCustomer(){
    this.customerList.push({});
    this.editableRow = this.customerList.length -1;
  }

}

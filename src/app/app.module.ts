import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{RouterModule}from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import{appRoutes}from './routerconfig';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { CustomersComponent } from './customers/customers.component';
import { CustomersService } from './customers/customers.service';

@NgModule({
  declarations: [
    AppComponent,
    CustomersComponent,
    
  ],
  imports: [
    BrowserModule,RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule
  ],
  providers: [
    CustomersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

 }

import { Routes } from '@angular/router';
import { CustomersComponent } from './customers/customers.component';

export const appRoutes: Routes = [
  {
    path: 'customers',
    component: CustomersComponent
  },
  {
    path: '',
    redirectTo: "customers",
    pathMatch: "full"
  }
];